# cxdbsql

The cxdbsql packages implements any storage interfaces defined in `cxdb`, as well as some interfaces in `match` using MySQL.
We may want to move all remaining interfaces from cxdb to match
