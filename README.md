# clockwork

[![License](http://img.shields.io/badge/License-MIT-brightgreen.svg)](./LICENSE)

# Setup

## Requirements

- Go 1.12+
- A MySQL Database (not needed for client)
- GMP (GNU Multiple Precision Arithmetic Library)

## Installing

### Installing GMP

#### Debian

```sh
sudo apt-get install libgmp3-dev
```

#### macOS

```sh
brew install gmp
```

### Clone repo and install dependencies

```sh
git clone https://gitlab.com/clockwork-paper/clockwork.git
cd clockwork
go get -v ./...
```

## Running opencx server / exchange

You will need to run MariaDB or any other MySQL database in-order to run the server. You can configure authentication details for your database at `~/.opencx/db/sqldb.conf`

### Start your database (MariaDB in this case)

#### Linux

```sh
sudo systemctl start mariadb
```

#### macOS

```sh
mysql.server start
```

### Now build and run opencx

```sh
go build ./cmd/opencxd/...
./opencxd
```

## Running opencx CLI client

```sh
go build ./cmd/ocx/...
./ocx
```
